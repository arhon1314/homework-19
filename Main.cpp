
#include <iostream>
#include <string>
using namespace std;

class animal
{
protected:

	string voice_of_animal = "growling";

public:

	animal() {}

	animal(string _voice_of_animal) : voice_of_animal(_voice_of_animal)
	{}
	
	virtual int Voice() = 0;
};

class raven : public animal
{
public:

	raven() {};

	raven(string _voice_of_animal) : animal(_voice_of_animal)
	{};

	int Voice() override
	{
		std::cout << "\n" << voice_of_animal << "\n";
		return 0;
	}

};

class dog : public animal
{
public:

	dog() {};

	dog(string _voice_of_animal) : animal(_voice_of_animal)
	{};
	
	int Voice() override
	{
		std::cout << "\n" << voice_of_animal << "\n";
		return 0;
	}

};


class cat : public animal
{
public:

	cat() {};

	cat(string _voice_of_animal) : animal(_voice_of_animal)
	{};

	int Voice() override
	{
		std::cout << "\n" << voice_of_animal << "\n";
		return 0;
	}
};


int main()
{
	animal* d = new raven("KAR");
	std::cout << d -> Voice();

	animal* b = new dog("Gav");
	std::cout << b -> Voice();

	animal* q = new cat("Meow");
	std::cout << q -> Voice();
}